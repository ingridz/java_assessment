public class Launcher {
    public static void main(String[] args) throws Exception {
        try {
            
            NoteStore notestore = new NoteStore();
            TextNote textnote = new TextNote();
            textnote.setNote("Java is a set of computer software and specifications developed by James Gosling at Sun Microsystems.");
            notestore.addNotes(textnote);
            textnote.printtext();
            textnote.setNote("Few books to read-lkigai,How to win friends and influence people");
            notestore.addNotes(textnote);
            textnote.printtext();

            TextAndImageNote imagenote = new TextAndImageNote();
            
            imagenote.setNote("The shopping list on my fridge,");
            imagenote.setUrl("//foo/bar1/bar2/imgset1.jpg");
            notestore.addNotes(imagenote);
            imagenote.printtext();
            imagenote.printurl();

            System.out.println();

            imagenote.setNote("The size label of Jack's shirt,");
            imagenote.setUrl("//foo/bar1/bar2/imgset2.jpg");
            notestore.addNotes(imagenote);
            imagenote.printtext();
            imagenote.printurl();

            
        

        } catch (Exception e) {

        }
    }

}
