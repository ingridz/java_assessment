

public class LauncherNew {
    public static void displayTextNotes(NoteStore n){
        n.getAllTextNotes();

    }
    public static void displayTextAndImageNotes(NoteStore n){
        n.getAllTextAndImageNotes();

    }
    public static void main(String[] args) throws Exception {
        try {
            
            
            NoteStore notestore = new NoteStore();
            TextNote textnote = new TextNote();
            textnote.setNote("Java is a set of computer software and specifications developed by James Gosling at Sun Microsystems.");
            notestore.storeNote(textnote);
            textnote.setNote("Few books to read-lkigai,How to win friends and influence people");
            notestore.storeNote(textnote);
          

            TextAndImageNote imagenote = new TextAndImageNote();
            
            imagenote.setNote("The shopping list on my fridge,");
            imagenote.setUrl("//foo/bar1/bar2/imgset1.jpg");
            notestore.storeNote(imagenote);
        

            imagenote.setNote("The size label of Jack's shirt,");
            imagenote.setUrl("//foo/bar1/bar2/imgset2.jpg");
            notestore.storeNote(imagenote);
          

            displayTextNotes(notestore);
            displayTextAndImageNotes(notestore);
        

        } catch (Exception e) {

        }
    }

}
