public class Notes {
    private String note;
    private String url;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void printtext() {
        System.out.println(this.note);
    }

    public void printtext(String note) {
        System.out.println(note);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void printurl() {
        System.out.print(this.url);
    }

    public void printurl(String url) {
        System.out.println(url);
    }


    
}
